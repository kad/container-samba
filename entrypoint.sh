#!/bin/sh -ex

if [[ ! -f "/etc/samba/.bootstrapped" ]]; then
  test -e /etc/samba/smb.conf && mv -v /etc/samba/smb.conf /etc/samba/smb.conf.orig
  mv -v /etc/smb.conf.default /etc/samba/smb.conf
  touch /etc/samba/.bootstrapped
fi

chmod -v 0700 /etc/monitrc

exec monit
